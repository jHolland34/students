My name is Xiao Zhou; I go by "Michelle" at school/work. I have a Bachelor's degree in Biomedical Engineering and I'm currently studying for my second one in Computer Science. 
I've worked a variety of jobs from waiting tables to scribing in the ER to programming in industry and academia. 
Outside of work and school, I'm a big foodie and I love trying out new flavors/restaurants.   